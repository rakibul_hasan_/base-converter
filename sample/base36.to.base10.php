<?php
require_once __DIR__ . '/../vendor/autoload.php';
use Base\FluentConvert;

function base36_to_base10($number)
{
    return FluentConvert::number($number)
                ->fromBase(36)
                ->toBase(10);
}


$base10 = base36_to_base10('0');
echo $base10;