<?php
/**
 * Created by PhpStorm.
 * User: Rakibul Hasan
 * Date: 5/14/2016
 * Time: 11:23 AM
 */

namespace Base;


class BaseConverter
{
    private static $digitToBaseDigitList = null;
    private static $baseDigitToDigitList = null;

    public static function convert($number, $fromBase, $toBase)
    {
        if ($toBase == $fromBase) {
            return $number;
        }
        //Or else
        $base10NumberFromGivenNumber = self::convertAnyBaseToBase10($number, $fromBase);
        $desiredBaseNumberFromBase10Number = self::convertBase10ToAnyBase($base10NumberFromGivenNumber, $toBase);
        return $desiredBaseNumberFromBase10Number;
    }


    public static function convertBase10ToAnyBase($base10Number, $toBase)
    {
        if ($toBase == 10) {
            return $base10Number;
        }
        return self::tailRecursiveConvert10ToAnyBase($base10Number, $toBase, '');
    }

    public static function convertAnyBaseToBase10($number, $fromBase)
    {
        if ($fromBase == 10) {
            return $number;
        }
        $number = strtolower($number);
        $base10Number = 0;
        $length = strlen($number);
        for ($i = 0; $i < $length; $i++) {
            $currentChr = $number[$length - $i - 1];
            $digit = self::getDigitFromBaseDigit($currentChr);
            $base10Number += $digit * ($fromBase ** $i);
        }
        return $base10Number;
    }

    private static function tailRecursiveConvert10ToAnyBase($currentNumber, $toBase, $resultSoFar)
    {
        if ($currentNumber < $toBase) {
            return strrev($resultSoFar . self::getBaseDigitFromDigit($currentNumber));
        }
        $reminder = $currentNumber % $toBase;
        $currentNumber = $currentNumber / $toBase;
        $newResult = $resultSoFar . self::getBaseDigitFromDigit($reminder);
        return self::tailRecursiveConvert10ToAnyBase($currentNumber, $toBase, $newResult);

    }

    private static function getDigitFromBaseDigit($baseDigit)
    {
        if (self::$digitToBaseDigitList == null) {
            self::populateVariable();
        }
        return self::$baseDigitToDigitList[$baseDigit];
    }

    private static function getBaseDigitFromDigit($digit)
    {
        if (self::$digitToBaseDigitList == null) {
            self::populateVariable();
        }
        return self::$digitToBaseDigitList[$digit];
    }

    private static function populateVariable()
    {
        self::$digitToBaseDigitList = [];
        self::$baseDigitToDigitList = [];
        for ($i = 0; $i < 36; $i++) {
            $baseDigit = self::getBaseDigit($i);
            self::$digitToBaseDigitList[] = $baseDigit;
            self::$baseDigitToDigitList[$baseDigit] = $i;
        }
    }

    private static function getBaseDigit($number)
    {
        if ($number >= 0 && $number < 10) {
            return '' . $number;
        }
        return chr($number + 87);
    }

}