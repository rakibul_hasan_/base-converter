<?php
/**
 * Created by PhpStorm.
 * User: Rakibul Hasan
 * Date: 5/14/2016
 * Time: 1:58 PM
 */

namespace Base;


class FluentConvert
{
    private $number = null;
    private $fromBase = null;

    public function __construct($number)
    {
        $this->number = $number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @param int $fromBase
     * @return FluentConvert
     */
    public function fromBase($fromBase)
    {
        $this->fromBase = $fromBase;
        return $this;
    }

    /**
     * @param int $toBase
     * @return FluentConvert
     */
    public function toBase($toBase)
    {
        return BaseConverter::convert($this->number,$this->fromBase,$toBase);
    }


    public static function number($number)
    {
        return new FluentConvert($number);
    }
}