# Base Converter#

Convert number from any base to any base. Max range is 36.

Sample file is in ***/sample/*** folder. Composer has been used in the project so use the command, 

`composer install`
